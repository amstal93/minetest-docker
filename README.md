# Minetest Docker images
This repository provides Dockerfiles for building Docker images with a [Minetest](https://github.com/minetest/minetest) server. The built images are available on [Docker Hub](https://hub.docker.com/r/webd97/minetestserver/).

The images are based on Alpine Linux to keep the them as small as possible.

## Quickstart
I assume that you are already quite familiar with Docker. However, please make sure you have proper security mechanisms set up on your host machine, as no system is error-free. Although Docker runs applications in isolation, it can till be abused to gain root access on your machine, if you don't secure your installation.

```
    $ sudo docker run \
      -p 30001:30000/udp \
      -v /home/webd97/minetest/worlds:/minetest/worlds \
      -v /home/webd97/minetest/minetest.conf:/minetest/minetest.conf \
      webd97/minetestserver:0.4.16
```

This will launch a stable 0.4.16 Minetest server with your host machine's `/home/webd97/minetest/worlds` mounted as world directory and `/home/webd97/minetest/minetest.conf` as minetest.conf (Make sure that this file exists __before__ starting the server). The container's UDP port 30000 will be forwarded to your host's UDP port 30001.

## What's included?
* LuaJIT is enabled
* minetest_game

For now, sqlite is the only supported database backend, additional ones might be available later.

## Available tags
### latest
Development build of the Minetest server with a development version of minetest_game. Updated manually. Please don't expect fresh commits to [minetest/minetest](https://github.com/minetest/minetest) to be available immediately.

    $ sudo docker run -p 30000:30000/udp webd97/minetestserver:latest

### 0.4.16
Stable 0.4.16 build with minetest_game 0.4.16

    $ sudo docker run -p 30000:30000/udp webd97/minetestserver:0.4.16

### 0.5.1
Stable 0.5.1 build with minetest_game 0.5.1

    $ sudo docker run -p 30000:30000/udp webd97/minetestserver:0.4.16

## Exposed ports
The images exposes port 30000/udp.

## Volumes
The images expose the following volumes. Remember to mount a directory of your host machine  to `/minetest/worlds` to persist your worlds!
* `/minetest/minetest.conf` for the configuration file ([minetest.conf](https://wiki.minetest.net/Minetest.conf))
* `/minetest/mods` for mods
* `/minetest/worlds` for worlds
* `/minetest/games` for subgames
