FROM alpine:latest

ARG MINETEST_VERSION

ADD minetest /opt/minetest
ADD minetest_game /opt/minetest/games/minetest_game

RUN apk update && apk add --no-cache \
	g++ \
	gcc \
	libc-dev \
	make \
	cmake \
	irrlicht-dev \
	libbz2 \
	libpng-dev \
	libjpeg-turbo-dev \
	libxxf86vm-dev \
	mesa-dev \
	sqlite-dev \
	curl-dev \
	zlib-dev \
	gmp-dev \
	luajit-dev \
	jsoncpp-dev && \
	\
	cd /opt/minetest && \
	cmake . \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DRUN_IN_PLACE=TRUE \
		-DBUILD_SERVER=TRUE \
		-DBUILD_CLIENT=FALSE \
		-DENABLE_CURL=TRUE \
		-DENABLE_FREETYPE=FALSE \
		-DENABLE_GETTEXT=FALSE \
		-DENABLE_SOUND=FALSE \
		-DENABLE_GLES=FALSE \
		-DENABLE_CURSES=FALSE \
		-DENABLE_LUAJIT=TRUE \
			-DLUA_INCLUDE_DIR=/usr/include/luajit-2.1 \
			-DLUA_LIBRARY=/usr/lib/libluajit-5.1.so \
		-DENABLE_SYSTEM_GMP=TRUE \
		-DENABLE_SYSTEM_JSONCPP=TRUE \
		-DVERSION_EXTRA=${MINETEST_VERSION} && \
	make -j $(getconf _NPROCESSORS_ONLN) && \
	rm -rf .git src build doc cmake util misc client lib fonts po clientmods CMakeFiles games/minimal CMakeLists.txt CMakeCache.txt CPackConfig.cmake CPackSourceConfig.cmake cmake_install.cmake Makefile && \
	\
	apk del wget make cmake gcc g++ irrlicht-dev

VOLUME /minetest/minetest.conf
VOLUME /minetest/mods
VOLUME /minetest/games
VOLUME /minetest/worlds

EXPOSE 30000/udp

ENTRYPOINT ["/opt/minetest/bin/minetestserver"]
CMD [""]
